$(document).ready(function(){ 

 

});


// menu
function openNav() {
   $('.sidenav').css('width','100%'); 
   $('body').css('overflow','hidden'); 
   $('body').css('height','100vh');
   $('body').css('position','relative');    
}

function closeNav() {
    $('.sidenav').css('width','0%');
    $('body').css('overflow','unset');
    $('body').css('height','unset');
    $('body').css('position','unset');     
}


function openform() {
    $('.start-new-project').css('width','100%'); 
    $('body').css('overflow','hidden'); 
    $('body').css('height','100vh');
    $('body').css('position','relative');    
 }
 
 function closeform() {
     $('.start-new-project').css('width','0%');
     $('body').css('overflow','unset');
     $('body').css('height','unset');
     $('body').css('position','unset');     
 }
// end


$(".heading-content").mouseover(function () {
    $('.menubar-header').addClass('header-hover-only');
    $('.banner-section').addClass('banner-section-image');
    $('.new-project').addClass('new-project-inner-page');
  }).mouseout(function () {
    $('.menubar-header').removeClass('header-hover-only');
    $('.banner-section').removeClass('banner-section-image');
    $('.new-project').removeClass('new-project-inner-page');
  });




  $(".technology-box a").mouseover(function () {
    $('.ourtechnology-section').addClass('hover-added');

  
  }).mouseout(function () {
    $('.ourtechnology-section').removeClass('hover-added');

  });


  $(".technology-box a.two").mouseover(function () {

    $('.bg-ourtechnology.two').css('opacity','1')
  
  }).mouseout(function () {
    $('.bg-ourtechnology.two').css('opacity','0')

  });

  $(".technology-box a.one").mouseover(function () {

    $('.bg-ourtechnology.one').css('opacity','1')
  
  }).mouseout(function () {
    $('.bg-ourtechnology.one').css('opacity','0')
  });

  $(".technology-box a.three").mouseover(function () {

    $('.bg-ourtechnology.three').css('opacity','1')
  
  }).mouseout(function () {
    $('.bg-ourtechnology.three').css('opacity','0')
  });

// welcome menu

$(".welcome-link").click(function(){
    $(".cb-menu-inner-welcome").hide();
    $(".cb-menu-inner").show();
});



// menu active
$(".cb-submenu-1 ul li").click(function(){
    $(".cb-submenu-1 ul li").removeClass('active');
    $(this).addClass('active');
});


// menu chnage
$('.cb-technolgy').click(function(){
    $('.cb-link').removeClass('active');
    $(this).addClass('active');
    $('.cb-submenu-1').hide();
    $('.cb-submenu-3').hide();
    $('.cb-submenu-2').show();

});

$('.cb-work').click(function(){
    $('.cb-link').removeClass('active');
    $(this).addClass('active');
    $('.cb-submenu-2').hide();
    $('.cb-submenu-3').hide();
    $('.cb-submenu-1').show();
});

$('.cb-service').click(function(){
    $('.cb-link').removeClass('active');
    $(this).addClass('active');
    $('.cb-submenu-1').hide();
    $('.cb-submenu-2').hide();
    $('.cb-submenu-3').show();
});



// owl carousel


$('.work-carousel').owlCarousel({
    loop:true,
    margin:2,
    nav:true,
    smartSpeed: 2000,
    dots:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/arrow-left.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/arrow-right.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4,
            dots:false,
            nav:true,
        }
    }
});



$('.testimonial-carousel').owlCarousel({
    loop:true,
    margin:0,
    stagePadding: 0,
    nav:true,
    smartSpeed: 2000,
    dots:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/arrow-left.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/arrow-right.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            dots:false,
            nav:true,
        },
      
    }
});




$('.img-slide-carousel').owlCarousel({
    loop:true,
    margin: 100,
    stagePadding: 300,
    nav:false,
    smartSpeed: 2000,
    dots:false,
    responsive:{
        0:{
            items:1,
            margin: 0,
            stagePadding: 0,  
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            dots:false,
            nav:true,
        }
    }
});


$('.related-technology').owlCarousel({
    loop:true,
    margin: 0,
    stagePadding: 0,
    nav:false,
    smartSpeed: 2000,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            dots:false,
            nav:false,
        }
    }
});



// owl-item-hover
$(".owl-item").mouseover(function () {
    $('body').addClass('owl-item-hover ');
  
  }).mouseout(function () {
    $('.body').removeClass('owl-item-hover');
   
});



